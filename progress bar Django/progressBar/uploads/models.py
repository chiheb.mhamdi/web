from django.db import models

# Create your models here.
class Uploads(models.Model) : 
    image=models.FileField(upload_to='pics')
    def __str__(self):
        return str(self.pk)