from django.shortcuts import render
from .forms import UploadsForm
from django.http import JsonResponse
# Create your views here.
def home(request):
    form=UploadsForm(request.POST or None, request.FILES or None)
    if request.is_ajax() : 
        if form.is_valid():
            form.save()
            return JsonResponse({'message':'hell yeah'})
    context={
        'form':form
    }
    return render(request,'base.html',context)