from django import forms
from django.db.models import fields 
from .models import Uploads 

class UploadsForm(forms.ModelForm):
    class Meta : 
        model = Uploads 
        fields=['image']