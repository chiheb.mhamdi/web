console.log('hello world')
const uploadForm = document.getElementById('upload-form')
const input = document.getElementById('id-video') 
const alertBox=document.getElementById('alert-box') 
const videoBox=document.getElementById('video-box')
const progressBox=document.getElementById('progress-box')
const cancelBox=document.getElementById('cancel-box')
const cancelBtn=document.getElementById('cancel-btn')
const csrf=document.getElementsByName('csrfmiddlewaretoken')



input.addEventListener('change',()=>{
    progressBox.classList.remove('not-visible')
    cancelBox.classList.remove('not-visible') 
    const video_data=input.files[0] 
    const url=URL.createObjectURL(video_data)
    console.log(url)
    const fd = new FormData() 
    fd.append('image',video_data)
    fd.append('csrfmiddlewaretoken',csrf[0].value)
    $.ajax({
        type:'POST',
        url:uploadForm.action,
        enctype:'multipart/form-data',
        data:fd,
        beforeSend:function(){
           
        },
        xhr:function(){
            const xhr=new window.XMLHttpRequest();
            xhr.upload.addEventListener('progress',e=>{
               // console.log(e)
               if(e.lengthComputable){
                   const percent = e.loaded / e.total * 100 
                   progressBox.innerHTML = `<div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: ${percent}%" aria-valuenow="${percent}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <p>${percent.toFixed(1)}
                    `
                   console.log(percent)
               }
            })
            cancelBtn.addEventListener('click',()=>{
                xhr.abort() 
                progressBox.innerHTML=""
                cancelBox.classList.add('not-visible')
            })
            return xhr
        },
        success:function(response){
            videoBox.innerHTML=`<video width="800" controls>
            <source src="${url}" type="video/mp4">
            </video> `

            alertBox.innerHTML=`<div class="alert alert-success" role="alert">
                                Successfully uploaded the video
                                </div>`

                cancelBox.classList.add('not-visible')

        },
        error: function(error){
            console.log(error)

        },
        cache:false,
        contentType:false,
        processData:false,
    })
})