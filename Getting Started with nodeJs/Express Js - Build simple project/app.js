const express=require('express') ; 
const app=express() ; 
app.get('/',function(req,res){
    res.send('HelloWorld')
})
app.get('/chiheb',function(req,res){
    res.send('Hello Chiheb')
})
app.get('/chiheb/:id',function(req,res)
{
    const id = req.params.id
    res.send('Hello chiheb '+ id)
})
app.get('/chiheb',function(req,res)
{
    const id = req.query.id
    res.send('Hello chiheb '+ id)
})
app.listen(9000,function(req,res)
{
console.log('Running .. ')    
})